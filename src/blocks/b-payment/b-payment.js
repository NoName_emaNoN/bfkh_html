//note: в примере используется библиотека jquery

this.pay = function () {
    var widget = new cp.CloudPayments({startWithButton: false});

    var data = { //данные дарителя
        firstName: $('#firstName').val(),
        lastName: $('#lastName').val(),
        middleName: $('#middleName').val(),
        phone: $('#phone').val(),
        comment: $('#comment').val()// + ' Куплено: ' + $('#count_pays').val() + ' билета(ов) по ' + $("#summ option:selected").val() + 'руб.'
    };

    console.log('data', data);

    widget.charge({ // options
            publicId: 'pk_c8c51678994099dcd16450a9eaee7',
            description: 'Пожертвование в Благотворительный Фонд Константина Хабенского',
            //publicId: 'test_api_00000000000000000000001',  //id из личного кабинета
            //description: 'Пример оплаты (деньги сниматься не будут)', //назначение
            amount: parseFloat($('#amount').val()), //сумма
            currency: 'RUB', //валюта
            //invoiceId: '1234567', //номер заказа  (необязательно)
            //accountId: 'user@example.com', //идентификатор плательщика (необязательно)
            accountId: $('#email').val(),
            email: $('#email').val(),
            data: data
        },
        function (options) { // success
            //действие при успешной оплате
            $('#info-success').addClass('in');
        },
        function (reason, options) { // fail
            //действие при неуспешной оплате
        });

    return false;
};

$(function () {
    /* Реакция на изменение предустановленной цены */
    $('[name="amount_list"]').on('change', function () {
        var value = $(this).val();

        if (value == 0) {
            $('#amount').closest('.form-group').removeClass('hidden');
        } else {
            $('#amount').closest('.form-group').addClass('hidden');
        }

        if (value == 0) {
            value = null;
        }

        $('#amount').val(value);
    });

    $('.b-payment__form').on('submit', pay);

    $('#phone').mask('+0 (000) 000-00-00', {placeholder: "+0 (000) 000-00-00"});
});